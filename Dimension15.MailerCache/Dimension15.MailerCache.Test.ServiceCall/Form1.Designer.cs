﻿namespace Dimension15.MailerCache.Test.ServiceCall
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.uiToAddress = new System.Windows.Forms.TextBox();
            this.uiFromAddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.uiSmtpServer = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.uiSmtpServerPort = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.uiSmtpServerAuthorisationName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.uiSmtpServerAuthorisationPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 227);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "To";
            // 
            // uiToAddress
            // 
            this.uiToAddress.Location = new System.Drawing.Point(93, 12);
            this.uiToAddress.MaxLength = 100;
            this.uiToAddress.Name = "uiToAddress";
            this.uiToAddress.Size = new System.Drawing.Size(185, 20);
            this.uiToAddress.TabIndex = 2;
            // 
            // uiFromAddress
            // 
            this.uiFromAddress.Location = new System.Drawing.Point(93, 38);
            this.uiFromAddress.MaxLength = 100;
            this.uiFromAddress.Name = "uiFromAddress";
            this.uiFromAddress.Size = new System.Drawing.Size(185, 20);
            this.uiFromAddress.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "From";
            // 
            // uiSmtpServer
            // 
            this.uiSmtpServer.Location = new System.Drawing.Point(93, 64);
            this.uiSmtpServer.MaxLength = 100;
            this.uiSmtpServer.Name = "uiSmtpServer";
            this.uiSmtpServer.Size = new System.Drawing.Size(185, 20);
            this.uiSmtpServer.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Smtp server";
            // 
            // uiSmtpServerPort
            // 
            this.uiSmtpServerPort.Location = new System.Drawing.Point(93, 90);
            this.uiSmtpServerPort.MaxLength = 5;
            this.uiSmtpServerPort.Name = "uiSmtpServerPort";
            this.uiSmtpServerPort.Size = new System.Drawing.Size(49, 20);
            this.uiSmtpServerPort.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Smtp server port";
            // 
            // uiSmtpServerAuthorisationName
            // 
            this.uiSmtpServerAuthorisationName.Location = new System.Drawing.Point(21, 140);
            this.uiSmtpServerAuthorisationName.MaxLength = 100;
            this.uiSmtpServerAuthorisationName.Name = "uiSmtpServerAuthorisationName";
            this.uiSmtpServerAuthorisationName.Size = new System.Drawing.Size(188, 20);
            this.uiSmtpServerAuthorisationName.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Smtp server auth user name";
            // 
            // uiSmtpServerAuthorisationPassword
            // 
            this.uiSmtpServerAuthorisationPassword.Location = new System.Drawing.Point(21, 185);
            this.uiSmtpServerAuthorisationPassword.MaxLength = 100;
            this.uiSmtpServerAuthorisationPassword.Name = "uiSmtpServerAuthorisationPassword";
            this.uiSmtpServerAuthorisationPassword.Size = new System.Drawing.Size(188, 20);
            this.uiSmtpServerAuthorisationPassword.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 169);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(135, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Smtp server auth password";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 262);
            this.Controls.Add(this.uiSmtpServerAuthorisationPassword);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.uiSmtpServerAuthorisationName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.uiSmtpServerPort);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.uiSmtpServer);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.uiFromAddress);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.uiToAddress);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox uiToAddress;
        private System.Windows.Forms.TextBox uiFromAddress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox uiSmtpServer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox uiSmtpServerPort;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox uiSmtpServerAuthorisationName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox uiSmtpServerAuthorisationPassword;
        private System.Windows.Forms.Label label6;
    }
}

