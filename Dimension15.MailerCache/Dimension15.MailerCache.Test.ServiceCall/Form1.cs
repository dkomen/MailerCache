﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dimension15.MailerCache.Test.ServiceCall
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dimension15.MailerCache.Entities.ScheduledMailMessage message = new Entities.ScheduledMailMessage();
            message.Body = "Test from MailerCache service: " + System.DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            message.ScheduleTime = System.DateTime.Now;
            message.Recipients.Add(new Entities.MailRecipient(uiToAddress.Text));
            message.MessageFrom = uiFromAddress.Text;
            message.SmtpServer = uiSmtpServer.Text;
            message.SmtpServerPort = int.Parse(uiSmtpServerPort.Text);
            message.SmtpServerAuthorisationName = uiSmtpServerAuthorisationName.Text;
            message.SmtpServerAuthorisationPassword = uiSmtpServerAuthorisationPassword.Text;
            message.Subject = "Test from MailerCache service";
            try
            {
                Dimension15.MailerCache.Service.Client.WcfClient.AddMessage(message);
                MessageBox.Show("Send success");
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex.Message);
            }
        }
    }
}
