﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Dimension15.MailerCache.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMailerCacheService" in both code and config file together.
    [ServiceContract]
    public interface IMailerCacheService
    {
        [OperationContract]
        void AddMessage(Dimension15.MailerCache.Entities.ScheduledMailMessage message);
    }
}
