﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Dimension15.MailerCache.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MailerCacheService" in both code and config file together.
    public class MailerCacheService : IMailerCacheService
    {
        public void AddMessage(Dimension15.MailerCache.Entities.ScheduledMailMessage message)
        {
            Mailer.GetInstance().AddMessage(message);
        }
    }
}
