﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Dimension15.MailerCache.Service
{
    public partial class Service : ServiceBase
    {        

        #region Fields
        private Dimension15.MailerCache.Mailer _mailer;
        private bool _stop = false;
        private ConsoleColor _standardColor = Console.ForegroundColor;
        #endregion

        public void Start(string[] args)
        {
            OnStart(args);

        }

        protected override void OnStart(string[] args)
        {
            System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(StartListening));
        }

        protected override void OnStop()
        {
            _stop = true;
            base.OnStop();
        }


        #region Private Functions

        private void ProcessingStatus(string message)
        {
            if (message.StartsWith("Subject: "))
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }
            WriteToConsole(System.DateTime.Now.ToString("dd MMM yyyy HH:mm:ss") + "[" + System.Threading.Thread.CurrentThread.ManagedThreadId + "]: " + message, _standardColor);
        }

        private void StartListening(object param)
        {

            new Dimension15.Services.Logging.WcfService.Logger().WatchDogNotificationController(true, 200);

            _mailer = Mailer.GetInstance();
            _mailer.ProcessingStatus += new ProcessingStatusDelegate(ProcessingStatus);
            Console.ForegroundColor = ConsoleColor.Gray;
            

            Uri baseAddress = new Uri("http://" + Dimension15.MailerCache.Service.Properties.Settings.Default.ServerAddress + ":" + Dimension15.MailerCache.Service.Properties.Settings.Default.Port.ToString() + "/Dimension15MailerCacheService");
            ServiceHost host = null;
            try
            {
                host = new ServiceHost(typeof(MailerCacheService), baseAddress);
                WriteToConsole("---------------------------------------------------------", _standardColor);
                WriteToConsole("Dimension15.MailerCache.Service", _standardColor);                
                Console.ForegroundColor = ConsoleColor.Red;
                WriteToConsole("Ensure you are running this app with appropriate rights (like Administrator)!!!", _standardColor);
                System.Threading.Thread.Sleep(2500);

                WriteToConsole("Listening on uri: " + baseAddress.ToString(), _standardColor);
                host.Open();
                WriteToConsole("Service is up and running!", _standardColor);
                Console.ForegroundColor = ConsoleColor.Yellow;
                WriteToConsole("Press any key to stop and exit", _standardColor);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                WriteToConsole("Error starting service: " + ex.ToString(), _standardColor);
                throw ex;
            }
            finally
            {
                Console.ForegroundColor = _standardColor;
            }
                        
            WriteToConsole("---------------------------------------------------------", _standardColor);
            do
            {
                System.Threading.Thread.Sleep(250);
            } while (_stop == false);
            host.Close();
        }
        private void WriteToConsole(string message, ConsoleColor standardConsoleTextColor)
        {
            if(message.ToLower().Contains("error"))
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            Console.WriteLine(message);
            Console.ForegroundColor = standardConsoleTextColor;
        }
            
        #endregion
    }
}
