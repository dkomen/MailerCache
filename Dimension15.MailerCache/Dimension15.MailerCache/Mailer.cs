﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.MailerCache
{

    public delegate void ProcessingStatusDelegate(string statusMessage);

    public class Mailer: IDisposable
    {
        #region Fields
        private static Mailer _instance;
        private static System.Timers.Timer _timer = null;
        private string _connectionString = Dimension15.MailerCache.Properties.Settings.Default.ConnectionString;
        private DORM.ConcreteAccessors.FluentNhibernate.Accessor<Entities.ScheduledMailMessage> _dal;
        private static object _threadLocker = "threadLocker";
        private static object _threadLockerDal = "threadLockerDal";
        #endregion

        #region Constructors
        private Mailer()
        {
            _dal = new DORM.ConcreteAccessors.FluentNhibernate.Accessor<Entities.ScheduledMailMessage>(_connectionString, DORM.Enumerations.DatastoreType.MsSql);
            _dal.CreateSessionFactory(true, _connectionString, DORM.Enumerations.DatastoreType.MsSql);     
            lock (_threadLocker)
            {
                if (_timer == null)
                {
                    _timer = new System.Timers.Timer(20000);
                    _timer.Elapsed += _timer_Elapsed;
                    _timer.Start();
                }
            }
        }

        public static Mailer GetInstance()
        {
            lock(_threadLocker)
            {
                if (_instance == null)
                {
                    _instance = new Mailer();
                }
            }
            return _instance;
        }

        #endregion

        #region Properties
        public ProcessingStatusDelegate ProcessingStatus { get; set; }
        #endregion

        /// <summary>
        /// Add a new fire-and-forget message to the system
        /// </summary>
        /// <param name="scheduledMessage"></param>
        /// <returns></returns>
        public bool AddMessage(Entities.ScheduledMailMessage scheduledMessage)
        {            
            using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
            {                                
                transaction.AddOrUpdate(scheduledMessage);
                transaction.Commit();
                return true;
            }
        }

        void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {            
            _timer.Stop();
            try
            {
                #region Get a list of all the pending ScheduledMailMessages
                IList<Entities.ScheduledMailMessage> scheduledMailMessages = new List<Entities.ScheduledMailMessage>();                
                using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate(true))
                {
                    DORM.Filter filter = new DORM.Filter();
                    DORM.FilterItem fItem = new DORM.FilterItem(null);
                    filter.Create(fItem.Add("ScheduleTime", DORM.Enumerations.FilterCriteriaComparitor.NotGreaterThan, System.DateTime.Now)).Add("SendComplete", DORM.Enumerations.FilterCriteriaComparitor.Equals, false);
                    scheduledMailMessages = transaction.Retrieve<Entities.ScheduledMailMessage>(filter, 100);
                }
                if (scheduledMailMessages.Count > 0)
                {
                    ProcessingStatus(string.Format("Found {0} messages to send", scheduledMailMessages.Count));
                }
                else
                {
                    ProcessingStatus(string.Format("No messages to send", scheduledMailMessages.Count));
                }
                #endregion

                System.Diagnostics.Stopwatch t = new System.Diagnostics.Stopwatch();
                t.Reset();
                t.Start();
                System.Threading.Tasks.ParallelOptions parallelOptions = new System.Threading.Tasks.ParallelOptions();
                parallelOptions.MaxDegreeOfParallelism = 15;
                System.Threading.Tasks.Parallel.ForEach<Entities.ScheduledMailMessage>(scheduledMailMessages, parallelOptions, scheduledMailMessage =>
                {
                    List<Entities.MailRecipient> successfullRecipients = new List<Entities.MailRecipient>();
                    try
                    {
                        System.Net.Mail.SmtpClient smtpMailClient = new System.Net.Mail.SmtpClient(scheduledMailMessage.SmtpServer, scheduledMailMessage.SmtpServerPort);

                        smtpMailClient.UseDefaultCredentials = false;
                        System.Net.NetworkCredential aCredential = new System.Net.NetworkCredential(scheduledMailMessage.SmtpServerAuthorisationName, scheduledMailMessage.SmtpServerAuthorisationPassword);
                        smtpMailClient.Credentials = aCredential;

                        ProcessingStatus("Subject: " + scheduledMailMessage.Subject);
                        ProcessingStatus("Smtp server: " + scheduledMailMessage.SmtpServer);

                        foreach (Entities.MailRecipient recipient in scheduledMailMessage.Recipients)
                        {
                            if (recipient.WasSent == false)
                            {
                                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                                message.To.Add(recipient.Address);
                                message.Subject = scheduledMailMessage.Subject;
                                message.Body = scheduledMailMessage.Body;
                                message.IsBodyHtml = true;
                                message.From = new System.Net.Mail.MailAddress(scheduledMailMessage.MessageFrom);
                                //Attachments not done yet!
                                smtpMailClient.Send(message);
                                ProcessingStatus(string.Format("Message sent to: " + message.To[0]));
                                recipient.WasSent = true;
                                successfullRecipients.Add(recipient);
                            }
                        }
                        using (DORM.Interfaces.ITransaction transaction2 = _dal.TransactionCreate())
                        {
                            lock (_threadLockerDal)
                            {
                                scheduledMailMessage.SendComplete = true;
                                transaction2.AddOrUpdate(scheduledMailMessage);
                                //transaction2.Delete(scheduledMailMessage);
                                transaction2.Commit();
                                successfullRecipients.Clear();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ProcessingStatus(string.Format("An error occurred : " + ex.ToString()));
                    }
                    finally
                    {
                        using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
                        {
                            if (successfullRecipients.Count != 0)
                            {
                                ProcessingStatus(string.Format("There was an error sending messages! Will try again on next run."));
                            }
                            lock (_threadLockerDal)
                            {
                                foreach (Entities.MailRecipient recipient in successfullRecipients)
                                {
                                    transaction.AddOrUpdate(recipient);
                                }
                                transaction.Commit();
                            }
                        }
                    }
                });
                t.Stop();
                if (scheduledMailMessages.Count > 0)
                {
                    ProcessingStatus(string.Format("Done sending {0} messages in {1} seconds at a speed of {2} messages per second", scheduledMailMessages.Count, (t.ElapsedMilliseconds / 1000), (float)scheduledMailMessages.Count / ((float)t.ElapsedMilliseconds / 1000)));
                }
            }
            catch(Exception ex)
            {
                ProcessingStatus(string.Format("There was an error sending messages! " + ex.ToString()));
            }
            finally
            {
                _timer.Start();
            }
        }

        public void Dispose()
        {
            _timer.Stop();
        }
    }
}
