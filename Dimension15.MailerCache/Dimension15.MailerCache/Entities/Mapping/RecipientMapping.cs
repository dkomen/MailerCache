﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.MailerCache.Entities.Mapping
{
    public class RecipientMapping: DORM.ConcreteAccessors.FluentNhibernate.EntityMap<Entities.MailRecipient>
    {
        public RecipientMapping()
        {
            Map(x=>x.Address).Not.Nullable().Length(200);
            Map(x => x.WasSent).Nullable();
            References<Entities.ScheduledMailMessage>(x=>x.ScheduledMailMessage).Nullable();
        }
    }
}
