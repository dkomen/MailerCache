﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.MailerCache.Entities.Mapping
{
    public class AttachmentMapping: DORM.ConcreteAccessors.FluentNhibernate.EntityMap<Entities.MailAttachment>
    {
        public AttachmentMapping()
        {
            Map(x=>x.Data).Not.Nullable().Length(1024*1024*25); //25MB
            Map(x => x.Name).Not.Nullable().Length(200);           
        }
    }
}
