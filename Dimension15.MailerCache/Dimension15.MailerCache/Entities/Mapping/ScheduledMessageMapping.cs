﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.MailerCache.Entities.Mapping
{
    public class ScheduledMessageMapping: DORM.ConcreteAccessors.FluentNhibernate.EntityMap<Entities.ScheduledMailMessage>
    {
        public ScheduledMessageMapping()
        {
            Map(x => x.Body).Not.Nullable().Length(1512);
            Map(x => x.MessageFrom).Not.Nullable().Length(150);
            Map(x => x.ScheduleTime).Not.Nullable();
            Map(x => x.SmtpServer).Not.Nullable().Length(150);
            Map(x => x.SmtpServerPort).Not.Nullable();
            Map(x => x.Subject).Not.Nullable().Length(200);
            Map(x => x.SmtpServerAuthorisationName).Length(100).Nullable();
            Map(x => x.SmtpServerAuthorisationPassword).Length(100).Nullable();
            Map(x => x.SendComplete).Default("false");
            HasMany<Entities.MailAttachment>(x => x.Attachments).Cascade.All();
            HasMany<Entities.MailRecipient>(x => x.Recipients).Cascade.All();
        }
    }
}
