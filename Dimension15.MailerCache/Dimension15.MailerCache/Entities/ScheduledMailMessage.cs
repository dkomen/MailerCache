﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.MailerCache.Entities
{
    public class ScheduledMailMessage: DORM.Interfaces.IEntity
    {
        #region Fields    
        #endregion

        #region Constructors
        public ScheduledMailMessage()
        {
            Recipients = new List<Entities.MailRecipient>();
            Attachments = new List<Entities.MailAttachment>();
        }
        #endregion

        public virtual long Id { get; set; }
        public virtual DateTime ScheduleTime { get; set; }
        public virtual string SmtpServer { get; set; }
        public virtual int SmtpServerPort { get; set; }
        public virtual string Subject { get; set; }
        public virtual string Body { get; set; }
        public virtual string MessageFrom { get; set; }
        public virtual IList<Entities.MailRecipient> Recipients { get; set; }
        public virtual IList<MailAttachment> Attachments { get; set; }
        public virtual string SmtpServerAuthorisationName { get;set; }
        public virtual string SmtpServerAuthorisationPassword { get; set; }
        public virtual bool SendComplete { get; set; }
    }
}
