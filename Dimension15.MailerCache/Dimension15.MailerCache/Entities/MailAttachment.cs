﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.MailerCache.Entities
{
    public class MailAttachment: DORM.Interfaces.IEntity
    {
        #region Fields
        #endregion

        #region Constructors
        public MailAttachment() { }
        public MailAttachment(string name, byte[] data) 
        {
            Name = name;
            Data = data;
        }
        #endregion

        public virtual long Id { get; set; }
        public virtual string Name { get; set; }
        public virtual byte[] Data { get; set; }
    }
}
