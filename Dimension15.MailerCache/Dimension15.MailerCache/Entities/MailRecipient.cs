﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.MailerCache.Entities
{
    public class MailRecipient: DORM.Interfaces.IEntity
    {

        #region Fields
        #endregion

        #region Constructors
        public MailRecipient(){}
        public MailRecipient(string address)
        {
            Address = address;
        }
        #endregion

        public virtual long Id { get; set; }
        public virtual bool WasSent { get; set; }
        public virtual string Address { get; set; }
        public virtual Entities.ScheduledMailMessage ScheduledMailMessage { get; set; }
    }
}
