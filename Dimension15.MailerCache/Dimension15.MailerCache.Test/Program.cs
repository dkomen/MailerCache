﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.MailerCache.Test
{
    class Program
    {

        private static Dimension15.MailerCache.Mailer _mailer = Mailer.GetInstance();

        static void Main(string[] args)
        {
            _mailer.ProcessingStatus += new ProcessingStatusDelegate(UpdateProcessingStatus);
            for (int loopCounter = 0; loopCounter < 1; loopCounter++)
            {
                Dimension15.MailerCache.Entities.ScheduledMailMessage message = new Entities.ScheduledMailMessage();
                message.ScheduleTime = System.DateTime.Now;
                message.Body = "Test message from mailer cache: " + System.DateTime.Now.ToString("yyyy MMM dd HH:mm:ss");
                message.Subject = "Here is a test message";
                message.SmtpServer = "smtp.hermesalert.com";
                message.SmtpServerPort = 25;
                message.MessageFrom = "noreply@hermesalert.com";
                message.Recipients.Add(new Dimension15.MailerCache.Entities.MailRecipient("dkomen@gmail.com"));
                //message.Recipients.Add(new Dimension15.MailerCache.Entities.MailRecipient("dean@hermesalert.com"));
                //message.Recipients.Add(new Dimension15.MailerCache.Entities.MailRecipient("dean@windingart.com"));
                _mailer.AddMessage(message);               
            }
            do{
                System.Threading.Thread.Sleep(1000);
            } while(1!=2);            
        }
        private static void UpdateProcessingStatus(string statusMessage)
        {
            Console.WriteLine(System.DateTime.Now.ToString("dd MM yyyy HH:mm:ss") + ": " + statusMessage);
        }
    }
}
